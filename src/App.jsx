import { useCallback, useRef, useState } from 'react';
import useBookSearch from './hooks/useBookSearch';

import './App.css'

function App() {
  const [query, setQuery] = useState('')
  const [pageNumber, setPageNumber] = useState(1)

  const { books, hasMore, loading, error } = useBookSearch(query, pageNumber)

  const observer = useRef()
  const lastBookElementRef = useCallback(node => {
    if (loading) return
    if (observer.current) observer.current.disconnect()
    observer.current = new IntersectionObserver(entries => {
      if (entries[0].isIntersecting && hasMore) {
        setPageNumber(prevPageNumber => prevPageNumber + 1)
      }
    })
    if(node) observer.current.observe(node)
  }, [loading, hasMore])

  const handleSearch = (e) => {
    setQuery(e.target.value)
    setPageNumber(1)
  }

  return (
    <div className='container py-5'>
      <input className="form-control my-4" type="text" name='search' placeholder="Search" aria-label="search" onChange={handleSearch}/>
      {
        books.map((book, index) => {
          if(books.length === index+1){
            return <div key={book} ref={lastBookElementRef}>{book}</div>
          }else{
            return <div key={book}>{book}</div>
          }
        })
      }
      
      {loading && <div>Loading...</div>}
      {error && <div>Error</div>}
      
    </div>
  )
}

export default App
